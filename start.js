const shell = require('shelljs');
const Promise = require('bluebird');

const downloadAllSessionScreenshots = require('./browserStackService').downloadAllSessionScreenshots;
const generateDiffs = require('./generateDiffs');
const commitAndPushScreenshots = require('commitAndPushScreenshots');
const archiveScreenshots = require('./archiveScreenshots');

// TODO: Move me to separate file
function driveBrowserStack() {
  const NIGHTWATCH_BINARY_PATH = '/home/jacob/testing/css_tests/node_modules/.bin/nightwatch';
  const BROWSERSTACK_CFG_PATH =  '/home/jacob/testing/css_tests/conf/single.conf.js';
  const AUTOMATED_TEST_SCRIPT = '/home/jacob/testing/css_tests/test_home.js';

  const BROWSERS_TO_RUN = [ 'chrome', 'firefox', 'safari', 'ie'].join(',');
  
  const CMD = `${NIGHTWATCH_BINARY_PATH} -c ${BROWSERSTACK_CFG_PATH} -e ${BROWSERS_TO_RUN} ${AUTOMATED_TEST_SCRIPT}`;

  return new Promise((resolve, reject) => {
    shell.exec(CMD, { async: true, silent: false }, (err, stdout) => {
      if (err) reject(err);
      resolve();
    });
  })
}

driveBrowserStack()
  .then(downloadAllSessionScreenshots)
  .then(archiveScreenshots)
  .then(generateDiffs)
  .then(commitAndPushScreenshots);
nightwatch_config = {
  selenium : {
    "start_process" : false,
    "host" : "hub-cloud.browserstack.com",
    "port" : 80
  },

  common_capabilities: {
    'build': 'nightwatch-browserstack',
    'browserstack.user': 'jacobschatz1',
    'browserstack.key': 'dmVmbXvtzvaSqkghzuxn',
    'browserstack.debug': true,
    'os': 'WINDOWS',
    'os_version': '10',
    'resolution': '1600x1200',
  },

  test_settings: {
   default: {},
    chrome: {
      desiredCapabilities: {
        browser: "Chrome",
      }
    },
    firefox: {
      desiredCapabilities: {
        browser: "Firefox",
      }
    },
    safari: {
      desiredCapabilities: {
        browser: "Safari",
      }
    },
    ie: {
      desiredCapabilities: {
        browser: "IE",
      }
    }
  }
};

// Code to support common capabilites
for(var i in nightwatch_config.test_settings){
  var config = nightwatch_config.test_settings[i];
  config['selenium_host'] = nightwatch_config.selenium.host;
  config['selenium_port'] = nightwatch_config.selenium.port;
  config['desiredCapabilities'] = config['desiredCapabilities'] || {};
  for(var j in nightwatch_config.common_capabilities){
    config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatch_config.common_capabilities[j];
  }
}

module.exports = nightwatch_config;

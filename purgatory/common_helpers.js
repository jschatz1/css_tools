module.exports = {
  beginTest(browser) {
    let url;
    url = browser.globals.app.url;
    return browser.url(url).maximizeWindow();
  }
};

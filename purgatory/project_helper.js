module.exports = {
  
  newProject(browser) {
    browser
      .setValue('#project_path','my_awesome_project')
      .setValue('textarea[name="project[description]"]', 'This is a fantastic project. There is no doubt. All other projects will scream and shout. What is love. Baby don\'t hurt me. Don\'t hurt me... No more... No more.')
      .click('.project-submit')
      
  }

}

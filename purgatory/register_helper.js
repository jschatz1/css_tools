
  register(browser) {
    let almostTherePage;
    let clearButton;
    let confirmAccountLink;
    let email;
    let emailConfirmBox;
    let emailConfirmMessage;
    let homePage;
    let letterLink;
    let mailUrl;
    let name;
    let newUserEmail;
    let newUserPassword;
    let newUsernameInput;
    let password;
    let refreshButton;
    let registerBox;
    let singUpButton;
    let userEmail;
    let username;
    let usernameInput;
    email = this.email;
    name = this.name;
    username = this.username;
    password = this.password;
    mailUrl = browser.globals.app.mailUrl;
    registerBox = `${loginPage} .pull-right .prepend-top-20 .new_new_user`;
    usernameInput = `${registerBox} #new_user_name`;
    newUsernameInput = `${registerBox} #new_user_username`;
    newUserEmail = `${registerBox} #new_user_email`;
    newUserPassword = `${registerBox} #new_user_password`;
    singUpButton = `${registerBox} .btn-create`;
    clearButton = '.btn.btn-danger';
    refreshButton = '.btn.refresh';
    userEmail = '.table-hover.letter-opener .active';
    letterLink = `${userEmail} a`;
    confirmAccountLink = 'p a';
    homePage = 'body#content-body';
    emailConfirmBox = `${homePage} .flash-container .flash-notice`;
    emailConfirmMessage = 'Your email address has been successfully confirmed.';
    commonHelpers.beginTest(browser);
    return browser
            .waitFor(loginPage)
            .waitFor(registerBox)
            .setValue(usernameInput, name)
            .setValue(newUsernameInput, username)
            .setValue(newUserEmail, email)
            .setValue(newUserPassword, password)
            .waitFor(singUpButton)
            .click(singUpButton)
            .pause(2500)
            .waitFor(homePage)
  }
}
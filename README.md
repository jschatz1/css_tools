TODO:

- Improve overall structure of code, so it's easier to work in
- Code screenshots by page title, not ID
- Only write diffs above certain percentage
- Include mobile browsers in automated tests
- Clean up the code, make it use-able
- Write project description in the README

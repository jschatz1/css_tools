const Promise = require('bluebird');
const axios = require('axios');
const async = require('async');


const apiAuth = {
  auth: {
   username: 'jacobschatz1',
   password: 'dmVmbXvtzvaSqkghzuxn',
  },
 };

export function downloadAllSessionScreenshots() {
  return browserStackService.fetchBuilds()
    .then(browserStackService.fetchSessions);
}

export function fetchBuilds() {
 return axios.get('https://www.browserstack.com/automate/builds.json', apiAuth);
}

export function fetchSessions(response) {
  const buildID = response.data[0].automation_build.hashed_id;
  const url = 'https://www.browserstack.com/automate/builds/<build-id>/sessions.json'.replace('<build-id>', buildID);

  return axios.get(url, config).then((sessions) => {
    const sessions_data = sessions.data;

    async.parallel([ 
      bindSessionTask.bind(this, sessions_data[0]),
      bindSessionTask.bind(this, sessions_data[1]),
      bindSessionTask.bind(this, sessions_data[2]),
      bindSessionTask.bind(this, sessions_data[3]),
      bindSessionTask.bind(this, sessions_data[4]),
    ]);
  });
}

export function downloadAllSessionScreenshots(sessions) {
  const sessions_data = sessions.data;
  
  const promises = [];

  sessions_data.forEach((session, idx) => {
    const promise = fetchSessionScreenshotLogs(session);
    promises.push(promise);
  });

  return Promise.all(promises);
}

function fetchSessionScreenshotLogs(session_data) {
  let url = `${session_data.automation_session.logs}.json`;
  const browsername = session_data.automation_session.browser;
  const browserversion = session_data.automation_session.browser_version;

  return axios.get(url, apiAuth)
    .then(scrapeScreenshotUrls)
    .then(downloadScreenshots.bind(null, browsername, browserversion))
}


function scrapeScreenshotUrls(response) {
  const awsUrlRegex = /((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)\w*.jpeg\b/g;
  const pwd = '/home/jacob/testing/screenshot-archive/';

  return response.data.match(awsUrlRegex);
}


function downloadScreenshots(browsername, browserversion, matches) {
  let i = 1, threads = 5, files = [], index, oid, repo;


  const promises = [];

  matches.forEach((url, idx) => {
    const filePath =  `${pwd}${browsername}_${browserversion}_id_${(idx)}.jpeg`;
    promises.push(downloadScreenshot(url, filePath));
  });

  return Promise.all(promises);
}

export function downloadScreenshot(url, dest, cb) {
  return new Promise((resolve, reject) => {
    const out = fs.createWriteStream(dest);

    const request = axios.get(url, function(response) {
      response.pipe(out);
      out.on('error', (err) => {
        reject(err);
      });
      out.on('finish', function() {
        out.close();
        resolve();
      });
    });
  });
}

const fs = require('fs');
const path = require('path');
const im = require('imagemagick');
const async = require('async');

const currentDirs = {};

const screenshotArchive = '/home/jacob/testing/screenshot-archive';

fs.readdir(screenshotArchive, (err, data) => {
	var dirContent = data;
	const lastIdx = dirContent.length - 1;
	const secondToLastIdx = dirContent.length - 2;

	const currentScreenshotSet = path.resolve(screenshotArchive, dirContent[lastIdx]);
	const previousScreenshotSet = path.resolve(screenshotArchive, dirContent[secondToLastIdx]);

	fs.readdir(currentScreenshotSet, (err, files) => {
		if (err) throw err;

		const converterSeries = [];

		files.forEach((filePath, idx) => {
			const fullPathForToday = path.resolve(currentScreenshotSet, filePath);			
			const pathForYesterday = path.resolve(previousScreenshotSet, filePath);
			const diffPath = `${fullPathForToday}.diff.png`;

			const imgMagikCommand = [ 
				`( ${fullPathForToday} -flatten -grayscale Rec709Luminance )`.split(' '), 
				`( ${pathForYesterday} -flatten -grayscale Rec709Luminance )`.split(' '), 
				`( -clone 0-1 -compose darken -composite )`.split(' '),
				`-channel RBG -combine ${diffPath}`.split(' '),
			].reduce((memo, cmdChunk) => {
				return memo.concat(cmdChunk);
			}, []);
	
			converterSeries.push(makeConverter(imgMagikCommand, diffPath));			
		});
	
			
		async.series(converterSeries, (err, results) => {
			console.log("Done!", err, results);
		});

	});
	// get all files in dir
	// loop over the list
	// find the equivalent one in previous set
	// diff them
	// output the diff file with the same path as current, but with attached
	// console.log(currentScreenshotSet, previousScreenshotSet);
});

function makeConverter(cmd, diffPath) {
	return (callback) => {
		im.convert(cmd, (err, stdout) => {
			//if (err) throw err;
			console.log(`Finished generating diff for ${diffPath}`);
			callback(null, diffPath);
		});
	};
}
// getStates of 
// newDirPath will be in screenshot archive

// parse the newFile for browser version screenshot ID
// get the previous folder
// find the match
// generate the diff


// convert '(' $FILE_1 -flatten -grayscale Rec709Luminance ')' '(' $FILE_2 -flatten -grayscale Rec709Luminance ')' '(' -clone 0-1 -compose darken -composite ')' -channel RGB -combine diff.png

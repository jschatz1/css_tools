// const loginHelpers = require('./user_helpers');

module.exports = {
  
  'GitLab New Project' : function (browser) {
	// login
	browser.url('https://gitlab.com/users/sign_in')
	.setValue('#user_login','fakeymcfakerson')
	.setValue('#user_password', 'protein condor choir best')
	.click('input[name="commit"]')

	// tour the application
	
	.url('https://gitlab.com/fakeymcfakerson/element')
	.url('https://gitlab.com/fakeymcfakerson/element/issues')
	.url('https://gitlab.com/fakeymcfakerson/element/issues/1')
	.url('https://gitlab.com/fakeymcfakerson/element/merge_requests/')
	.url('https://gitlab.com/fakeymcfakerson/element/merge_requests/1/')

	// logout
	.waitForElementVisible('.header-user-dropdown-toggle', 1000)	
	.click('.header-user-dropdown-toggle')
	.click('.sign-out-link')
	.end();
  }
};

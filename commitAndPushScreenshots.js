const shell = require('shelljs');

const SCREENSHOT_ARCHIVE_PATH = 'you need to get this';

export default function commitAndPushScreenshots() {
  return new Promise((resolve, reject) => {
    const git = '/usr/local/bin/git';
    const remote = 'origin';
    const branch = 'master';

    shell.exec(`
      cd ${SCREENSHOT_ARCHIVE_PATH} &&      
      ${git} add . && 
      ${git} commit -m ${commitMessage} && 
      ${git} push ${remote} ${branch}`, { async: true, silent: false }, (err, stdout) => {
        if (err) reject(err);
        resolve(stdout);
      });
  });
}

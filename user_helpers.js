const commonHelpers = require('./common_helpers');

const loginPage = '.login-page .navless-container';
const homePage  = '.page-with-sidebar';

module.exports = {
  user: 'root',
  password: 'byzantineempire123',
  name: 'Jeff Sessions',
  email: 'sessions@sessions.com',
  

  login(browser) {
    browser.url('https://gitlab.com/users/sign_in')
      .waitForElementVisible('body', 1000)
      .setValue('#user_login','fakeymcfakerson')
      .setValue('#user_password', 'protein condor choir best')
      .click('input[name="commit"]')
  },

  logout(browser) {
    let logoutItem = '.header-content .collapse .logout';

    return browser
	    .waitForElementVisible(logoutItem)
      .click(logoutItem)
      .waitForElementVisible(loginPage);
  },
};

const shell = require('shelljs');

const SCREENSHOT_ARCHIVE_PATH = 'you need to get this';

export default function commitAndPushScreenshots() {
  return new Promise((resolve, reject) => {
    const git = '/usr/local/bin/git';
    const remote = 'origin';
    const branch = 'master';

    shell.exec(`
      cd ${SCREENSHOT_ARCHIVE_PATH}
      NEW_DIR_NAME="screenshots_$(date +%Y-%m-%d-%T)_$(git branch | grep \* | cut -d ' ' -f2)_$(git rev-parse --verify HEAD)"
      mkdir $NEW_DIR_NAME
      mv ./*.jpeg $NEW_DIR_NAME`, { async: true, silent: false }, (err, stdout) => {
        if (err) reject(err);
        resolve(stdout);
      });
  });
}